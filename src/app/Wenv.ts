import {app, Menu, Tray} from "electron";

export default class Wenv {

    appIcon: Tray;

    contextMenu: Menu;

    template: object[] = [
        {
            click: () => {
                this.start();
            },
            label: "Start",
        },
        {
            click: () => {
                this.restart();
            },
            enabled: false,
            label: "Restart",
        },
        {
            click: () => {
                this.stop();
            },
            enabled: false,
            label: "Stop",
        },
        {
            type: "separator",
        },
        // {
        //     click: () => {
        //         this.restart();
        //     },
        //     label: "Settings",
        // },
        {
            click: () => {
                this.stop();
                app.quit();
            },
            label: "Quit",
        },
    ];

    constructor(iconPath: string) {
        this.appIcon = new Tray(iconPath);
        this.contextMenu = Menu.buildFromTemplate(this.template);
    }

    start() {
       console.log("start");
    }

    stop() {
        console.log("stop");
    }

    restart() {
        this.stop();
        this.start();
    }
}
