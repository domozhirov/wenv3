// Modules to control application life and create native browser window
import {app} from "electron";
import * as path from "path";
import Wenv from "./Wenv";

const icon: string = require("../assets/icons/16.png");

let wenv: Wenv;

if (process.platform === "darwin") {
    app.dock.hide();
}

function createWindow() {
    wenv = new Wenv(icon);
}

app.on("ready", createWindow);

// Quit when all windows are closed.
app.on("window-all-closed", () => {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== "darwin") {
        app.quit();
    }
});

app.on("activate", () => {
    // On OS X it"s common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    // if (mainWindow === null) {
    //     createWindow();
    // }
});

// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.